package org.sample.client.stage.impl;

import com.jme3.app.state.AbstractAppState;
import org.sample.client.stage.Stage;

/**
 * Created by ronn on 23.01.16.
 */
public abstract class AbstractStage extends AbstractAppState implements Stage {
}
