package org.sample.client.ui.component.login;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import org.sample.client.ui.component.ScreenComponent;
import rlib.ui.util.FXUtils;

/**
 * Реализация компонента авторизации.
 *
 * @author Ronn
 */
public class LoginAuthPanel extends StackPane implements ScreenComponent {
    public LoginAuthPanel() {
        this.setAlignment(Pos.CENTER);
        FXUtils.setFixedSize(this, 300, 300);
        loginComponents();
    }

    public void loginComponents() {

        VBox vBoxLogin = new VBox();
        vBoxLogin.setAlignment(Pos.CENTER);
        FXUtils.addToPane(vBoxLogin, this);
        vBoxLogin.setVisible(true);

        VBox vboxCreate = new VBox();
        vboxCreate.setAlignment(Pos.CENTER);
        FXUtils.addToPane(vboxCreate, this);
        vboxCreate.setVisible(false);

        TextField textField = new TextField();
        textField.setId("textField");
        textField.setPromptText("Login (Email)");
        FXUtils.addToPane(textField, vBoxLogin);
        VBox.setMargin(textField, new Insets(2, 2, 2, 2));

        PasswordField passwordField = new PasswordField();
        passwordField.setId("passwordField");
        passwordField.setPromptText("Password");
        FXUtils.addToPane(passwordField, vBoxLogin);
        VBox.setMargin(passwordField, new Insets(2, 2, 2, 2));

        Button button = new Button("Login");
        button.setId("button");
        FXUtils.addToPane(button, vBoxLogin);
        VBox.setMargin(button, new Insets(2, 2, 2, 2));

        Button buttonLabel = new Button("Create account");
        buttonLabel.setId("buttonLabel");
        buttonLabel.setOnAction(event -> {
            //TODO move to separate method
            vBoxLogin.setVisible(false);
            vboxCreate.setVisible(true);
        });
        FXUtils.addToPane(buttonLabel, vBoxLogin);
        VBox.setMargin(buttonLabel, new Insets(2, 2, 2, 2));

/* Account creation ui elements */

        Button buttonCreate = new Button("Create account");
        buttonCreate.setId("button");
        FXUtils.addToPane(buttonCreate, vboxCreate);
        VBox.setMargin(buttonCreate, new Insets(2, 2, 2, 2));

        Button buttonBack = new Button("Back");
        buttonBack.setId("buttonLabel");
        buttonBack.setOnAction(event -> {
            vBoxLogin.setVisible(true);
            vboxCreate.setVisible(false);
        });
        FXUtils.addToPane(buttonBack, vboxCreate);
        VBox.setMargin(buttonBack, new Insets(2, 2, 2, 2));

    }
}
