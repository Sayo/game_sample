package org.sample.client.ui.scene.builder.impl;

import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import org.sample.client.ui.component.login.LoginAuthPanel;
import org.sample.client.ui.scene.UIScene;

import javafx.scene.layout.StackPane;
import rlib.ui.util.FXUtils;

/**
 * Реализация конструктора сцены логина.
 * 
 * @author Ronn
 */
public class LoginUISceneBuilder extends AbstractUISceneBuilder {

	/**
	 * Построение UI авторизации.
	 */
	protected void build(final UIScene scene, final StackPane root) {
		Pane backgroundPane = new Pane();
		backgroundPane.setStyle("-fx-background-color: #111515");
		FXUtils.addToPane(backgroundPane, root);
		FXUtils.bindFixedSize(backgroundPane, root.widthProperty(), root.heightProperty());

		LoginAuthPanel authPanel = new LoginAuthPanel();
		FXUtils.addToPane(authPanel, root);
	}
}
