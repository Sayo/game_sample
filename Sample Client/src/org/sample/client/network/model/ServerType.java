package org.sample.client.network.model;

/**
 * Перечисление типов серверов.
 *
 * @author Ronn
 */
public enum ServerType {
    GAME_SERVER;
}
