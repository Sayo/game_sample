package org.sample.server;

import org.sample.server.database.AccountDBManager;
import org.sample.server.database.DataBaseManager;
import org.sample.server.manager.AccountManager;
import org.sample.server.manager.ClassManager;
import org.sample.server.manager.ExecutorManager;
import org.sample.server.network.Network;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import rlib.Monitoring;
import rlib.compiler.CompilerFactory;
import rlib.concurrent.util.ThreadUtils;
import rlib.logging.Logger;
import rlib.logging.LoggerLevel;
import rlib.logging.LoggerManager;
import rlib.logging.impl.FolderFileListener;
import rlib.manager.InitializeManager;
import rlib.monitoring.MemoryMonitoring;
import rlib.monitoring.MonitoringManager;
import rlib.util.Util;

/**
 * Модель игрового сервера.
 *
 * @author Ronn
 */
public class GameServer extends ServerThread {

    private static final Logger LOGGER = LoggerManager.getLogger(GameServer.class);

    /**
     * Экземпляр гейм сервера.
     */
    private static GameServer instance;

    private static void configureLogging() {

        LoggerLevel.DEBUG.setEnabled(false);
        LoggerLevel.ERROR.setEnabled(true);
        LoggerLevel.WARNING.setEnabled(true);
        LoggerLevel.INFO.setEnabled(true);

        final Path logFolder = Paths.get(Config.FOLDER_PROJECT_PATH, "log");

        if (!Files.exists(logFolder)) {
            try {
                Files.createDirectories(logFolder);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        LoggerManager.addListener(new FolderFileListener(logFolder));
    }

    /**
     * @return экземпляр гейм сервера.
     */
    public static GameServer getInstance() {

        if (instance == null) {
            instance = new GameServer();
        }

        return instance;
    }

    public static void main(final String[] args) throws Exception {

        if (!CompilerFactory.isAvailableCompiler()) {
            throw new RuntimeException("not found java compiler.");
        }

        Monitoring.init();
        Config.init(args);

        configureLogging();

        Util.checkFreePort("*", Config.SERVER_PORT);

        final DataBaseManager manager = DataBaseManager.getInstance();
        manager.clean();

        getInstance();

        System.gc();
    }

    public GameServer() {
        setName("Initialize Thread");
        start();
    }

    @Override
    public void run() {
        try {

            InitializeManager.register(ExecutorManager.class);
            InitializeManager.register(DataBaseManager.class);
            InitializeManager.register(AccountDBManager.class);
            InitializeManager.register(IdFactory.class);
            InitializeManager.register(AccountManager.class);
            InitializeManager.register(ClassManager.class);
            InitializeManager.register(Network.class);
            InitializeManager.initialize();

            LOGGER.info("started.");

            final Runnable target = () -> {

                final MonitoringManager manager = MonitoringManager.getInstance();
                final MemoryMonitoring memory = manager.getMemoryMonitoring();

                while (true) {
                    LOGGER.info(memory, memory.getHeapUsedSize() / 1024 / 1024 + " MiB.");
                    ThreadUtils.sleep(30000);
                }
            };

            final ServerThread thread = new ServerThread(target);
            thread.setDaemon(true);
            thread.setName("Monitoring Memory");
            // thread.start();

        } catch (final Exception e) {
            LOGGER.warning(e);
            System.exit(0);
        }
    }
}
