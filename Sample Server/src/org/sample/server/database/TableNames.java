package org.sample.server.database;

/**
 * Created by ronn on 27.01.16.
 */
public interface TableNames {

    public interface AccountTable {
        public static final String TABLE_NAME = "account";
        public static final String ID = "id";
        public static final String NAME = "name";
        public static final String PASSWORD = "password";
    }
}
